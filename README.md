# Memory RPG - Frontend

This project is, above all, a self-learning project where libraries, coding practices, ... will be tested. It may or may not end on being something usable. So if you are wondering why there is some pen-test on an unused project ? Well ... that's the actual point for now ♥

That being said, the project should be an helper for a role-playing game where main gameplay is based on the fact that characters has lost their memories : players will then discover fragment along their RP sessions.

Issues are being managed with [Jira](https://w31q1.atlassian.net/jira/software/c/projects/MRPG/boards/1).

- [Memory RPG - Frontend](#memory-rpg---frontend)
  - [Quick startup](#quick-startup)
    - [Pre-requisites](#pre-requisites)
    - [Run the application](#run-the-application)
    - [Project documentation & contribution](#project-documentation--contribution)

## Quick startup

>___
> 📝 **Info**
> ___
> This project has been developed on VS Code on Windows. Feel free to use whatever you want but the documentation may not be always helpful !
> ___

### Pre-requisites

Before starting one may have :

- Installed [Node.js v16.13.2](https://nodejs.org/en/download/) (LTS).
- Cloned the [memory-rpg frontend repository](https://framagit.org/memory-rpg/frontend).

### Run the application

For a quick start :

1. Start a bash ;
2. Move to the application code root ;
    >___
    > 📝 Application root is `frontend/app/` folder.
    > ___
    > 
3. Execute commands :

    ```bash
    npm install
    npm run dev
    ```
>___
> ✔️ You can now access the application [home page](http://localhost:5000/).
> ___


### Project documentation & contribution

- [Main documentation page](documentation/INDEX.md).
- Develop & tests :
  - Everything is detailed in the [Developer guide](documentation/developer/developer_guide.md).
- Contribute
  - [Contribute to code](documentation/developer/coding_guide.md).
  - [Contribute to documentation](documentation/CONTRIBUTE.md).

