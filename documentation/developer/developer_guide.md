# Developer Guide

That documentation guide any person willing to work on the project.

- [Developer Guide](#developer-guide)
  - [Installation](#installation)
    - [Pre-requisites](#pre-requisites)
    - [Run the application](#run-the-application)
    - [Code checks](#code-checks)
      - [Manual checks](#manual-checks)
      - [Pipelines](#pipelines)
  - [Code contribution](#code-contribution)
  - [More](#more)
    - [Known issues](#known-issues)
    - [Resources](#resources)

## Installation

### Pre-requisites

**Mandatory** :

- Installed [Node.js v16.13.2](https://nodejs.org/en/download/) (LTS).
- Cloned the [memory-rpg frontend repository](https://framagit.org/memory-rpg/frontend).
**Recommended** :

- [VS Code (latest)](https://code.visualstudio.com/)
- VS Code plugins :
  - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) : A complete linter for Markdown (auto-format, problem detection, table of content, ...).
  - [Svelte for VS Code](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode) : linter for Svelte.


### Run the application

For a quick start :

1. Start a bash ;
2. Move to the application code root ;
    >___
    > 📝 Application root is `frontend/app/` folder.
    > ___
    > 
3. Execute commands :

    ```bash
    npm install
    npm run dev
    ```
>___
> ✔️ You can now access the application [home page](http://localhost:5000/).
> ___

The application will detect your code changes and automatically reload.

### Code checks

#### Manual checks

Code checks should be ran from the root of the repository.

>___
> 📝 **tl;dr : simply all commands**
> ___
> ```bash
> npm run cy:integ
> ```
> ___

#### Pipelines

So far the pipeline will run `cypress` against `cypress/integration/standalone` tests.

## Code contribution

>___
>❌🗋 **That section needs to be completed.**
>___

## More

### Known issues

>___
>❌🗋 **That section needs to be completed.**
>___

### Resources

Below links are good resources regarding that project :

- [Svelte documentation / tutorial](https://svelte.dev/)
- [Routify documentation](https://v1.routify.dev/)
    >___
    > ⚠️ Used routify version is `v2` but the documentation is a bit buggy :)
    > ___
- [Cypress documentation](https://docs.cypress.io/guides/overview/why-cypress)

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.
