# Project Code organization

Quick overview :

- All the code can be found under : `frontend/app/src`
- All app configuration, dependencies, tests, ... can be found under `frontend/app/`
- All repositoruy configuration, pipelines, documentation, ... can be found under `frontend/`

## Table of contents

- [Project Code organization](#project-code-organization)
  - [Table of contents](#table-of-contents)
  - [Content review](#content-review)
    - [src/](#src)
    - [pages/](#pages)
  - [Code/Pages versioning](#codepages-versioning)
  - [Components](#components)
  - [Public pages vs Private pages](#public-pages-vs-private-pages)

## Content review

### src/

```txt
app/src/                # Contains GLOBAL application elements (like global.css or App.svelte)
|_ App.svelte           # Application entry point
|_ main.js              # Declaration of App (as a routify app)
|_ global.css           # Define our application general design
|_ pages/               # Application pages (see next section)
```

### pages/

Routify generate routes (`/about`, `/games/`, `/my-profile`) automatically based on our folder tree structure :
![Routify pages structure from their main page.](.img/routify_tree_logic.PNG)

This is quite smooth and offers some helpful mechanism like :

- Code versioning (detailled in next section).
- Variable scoping.
- Global mechanism (see Public pages vs Private pages section).

>___
> 💡 **Tips about routify implicit**
> ___
> A `pages/folder/index.svelte` page will always refer to the route `http://mydomain/blabla/folder/`.
> ___


So far the choice of the project is to have some pages at the root :

| Page                                             | Description                           |
| :----------------------------------------------- | :------------------------------------ |
| [index.svelte](../../app/src/pages/index.svelte) | The root page `/` of the application. |
| [about.svelte](../../app/src/pages/about.svelte) | General info about the project.       |
| [login.svelte](../../app/src/pages/login.svelte) | As a globally accessible page.        |

As well as some "technical" files :

| File                                                     | Description                                                               |
| :------------------------------------------------------- | :------------------------------------------------------------------------ |
| [_store.js](../../app/src/pages/_store.js)               | A store for variables available for all application.                      |
| [_layout.svelte](../../app/src/pages/_layout.svelte)     | A layout to be applied to **all** pages (can be overrided in subfolders). |
| [_fallback.svelte](../../app/src/pages/_fallback.svelte) | A default fallback (404) page (can be overrided in subfolders).           |

Making `login.svelte` and `_store.js` at root was done to simplify pathing for other which may requires it (like avoiding `$goto(../../../login/index.svelte)` as much as possible :') )

Then you will find folders named `vX` where `X` can be a value like `1` or `5.3`. See [next section](#code-versioning) for details.

## Code/Pages versioning

Pages are being versioned to allows future aliasing (see [an example](https://github.com/roxiness/routify-starter/tree/master/src/pages/example/aliasing) on routify-starter git).

For now, everything is `v1` and while the application has no "first release", there is no point about having aliased pages.

## Components

For now, components are stored under `v1/_components` : it is not clear if they should follow pages versioning or not !

>___
>❌🗋 **That section needs to be completed.**
>___

## Public pages vs Private pages

We consider that pages **directly** under `pages/` folder are accessible even for not authenticated users.

Pages under `pages/v1/` are controlled by [pages/_layout.svelte](../../app/src/pages/_layout.svelte) which redirect to the `login/` page is no user token is found.

>___
> 📝 **Info**
> ___
> The "private" pages are not "secured" : one may let application think we have a token (even if invalid). It is more about user experience : if one connect to a page without token, API will send `401` codes on its queries and final page will not be very clean !
> ___

In future, a token validity maybe automatically check (and the token refreshed if needed) : best pattern to that should be checked.

>___
> ⚠️ **Warning**
> ___
> The auto-redirect to login page is managed under `v1/_layout.svelte` but that is to be improved as we may be forced to duplicate it [for each alias](#codepages-versioning) in the future.  
> On the other hand, to make it available for all children, we need kind of subfolder which has impact on routing : a solution is still to be found on that !
> ___


> ___
> ↩️ Browse back to [Summary](./INDEX.md) page.
