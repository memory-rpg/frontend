# Application documentation


## Current section documentation files

Documentation in that folder: 
| File                             | Description                                          |
| :------------------------------- | :--------------------------------------------------- |
| [../](../README.md)              | Back to previous documentation level.                |
| [INDEX.md](./INDEX.md)           | Current file.                                        |
| [CONTRIBUTE.md](./CONTRIBUTE.md) | Learn how-to make this place a better documentation. |

## Sub-sections documentation
| Folder                            | Description                                                                          |
| :-------------------------------- | :----------------------------------------------------------------------------------- |
| [Developer](./developer/INDEX.md) | Everything regarding environment setup, coding rules, snippets, local utilities, ... |

>___
> This is the end of the section : go back to [previous section](../README.md). 🔚
