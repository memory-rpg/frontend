import { user_demo_1 } from "../../fixtures/users"

describe('Signin from home page', () => {
    beforeEach(() => {
        cy.intercept("POST", "/token", { fixture: "token.json" })
    })
    it('Home (/)', () => {
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })

        cy.visit('http://localhost:5000/')

        cy.contains("Sign in to Memory RPG")
        cy.get("#login-component form").within(() => {
            cy.get('input[name="username"]').type(user_demo_1.username)
            cy.get('input[name="password"]').type(user_demo_1.password)

            cy.contains("Sign in").click()
        })

        cy.get("#login-component form").should('not.exist');
        cy.contains("Your games")

        cy.url().should("include", "/")
    })
})
