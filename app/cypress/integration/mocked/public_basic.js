describe('Browse public pages', () => {
    it('Home (/)', () => {
        cy.visit('http://localhost:5000/')
        cy.contains("Sign in to Memory RPG")
        cy.contains("Create an account")
    })

    it('About (/about)', () => {
        cy.visit('http://localhost:5000/about')
    })
})

describe('Access expected 404', () => {
    it('Home (/home)', () => {
        // "no /home page"
        cy.visit('http://localhost:5000/home')
        cy.contains("404")
    })
})

describe('Navigation in public pages', () => {
    it('From / to /about', () => {
        cy.visit('http://localhost:5000/')

        cy.contains("About Memory RPG").click()
        cy.url().should("include", "/about")
    })
})