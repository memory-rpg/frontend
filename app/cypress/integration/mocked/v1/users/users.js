import { user_demo_1 } from "../../../../fixtures/users"

describe("/users : Check access (and redirect to /users/me)", () => {

    beforeEach(() => {
        cy.intercept("POST", "/token", { fixture: "token.json" })
        cy.intercept("GET", "/users/me", {fixture: "user.json"})
    })

    it("Direct access", () => {
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        // Redirected to login but URL should still be targeted "users/" page
        cy.visit("http://localhost:5000/v1/users")

        // Redirected to login but URL should still be targeted "users/" page
        cy.url().should("include", "v1/users")
        cy.get('input[name="username"]').type(user_demo_1.username)
        cy.get('input[name="password"]').type(user_demo_1.password)
        cy.get("form").contains("Sign in").click()

        cy.url().should("include", "v1/users/me")
    })
})

describe("/users/me : Check user profile access", () => {

    beforeEach(() => {
        cy.intercept("POST", "/token", { fixture: "token.json" })
        cy.intercept("GET", "/users/me", { fixture: "user.json" })
    })

    it("Direct access", () => {
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        // Redirected to login but URL should still be targeted "users/" page
        cy.visit("http://localhost:5000/v1/users/me")

        // Redirected to login but URL should still be targeted "users/" page
        cy.url().should("include", "v1/users/me")
        cy.get('input[name="username"]').type(user_demo_1.username)
        cy.get('input[name="password"]').type(user_demo_1.password)
        cy.get("form").contains("Sign in").click()

        cy.url().should("include", "v1/users/me")
    })
})