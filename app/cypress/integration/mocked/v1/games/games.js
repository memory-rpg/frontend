import { user_demo_1 } from "../../../../fixtures/users"

describe("/games : Check is 404", () => {

    beforeEach(() => {
        cy.intercept("POST", "/token", { fixture: "token.json" })
        cy.intercept("GET", "/users/me", {fixture: "user.json"})
    })

    it("Direct access", () => {
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        // Redirected to login but URL should still be targeted "games/" page
        cy.visit("http://localhost:5000/v1/games")


        cy.url().should("include", "v1/games")
        cy.contains("404")
    })
})

describe("/games/1 : Access any game", () => {

    beforeEach(() => {
        cy.intercept("POST", "/token", { fixture: "token.json" })
        cy.intercept("GET", "/users/me", {fixture: "user.json"})
    })

    it("Direct access", () => {
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        // Redirected to login but URL should still be targeted "games/" page
        cy.visit("http://localhost:5000/v1/games/1")

        cy.wait(500) // small pause as cypress sometimes confuse input with home login form component
        cy.url().should("include", "v1/games/1")
        cy.get('input[name="username"]').type(user_demo_1.username)
        cy.get('input[name="password"]').type(user_demo_1.password)
        cy.get("form").contains("Sign in").click()

        // We should have been redirected to current the game 1 page
        cy.url().should("include", "v1/games/1")
    })
})