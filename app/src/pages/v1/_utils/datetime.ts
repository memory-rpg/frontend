export function formatTimestampToString(timestamp: number, hours: Boolean = true) {
    let date = new Date(timestamp * 1000);

    let formattedDate = (date.getDate() +
        "/" + (date.getMonth() + 1) +
        "/" + date.getFullYear());

    if (hours) {
        formattedDate += (
            " " + date.getHours() +
            ":" + date.getMinutes() +
            ":" + date.getSeconds());
    }
    return formattedDate
}