/**
 * Centralized code with functions managing API calls.
 */
const TargetProtocol = "http";
const TargetName = "localhost";
const TargetPort = 8000;

import { get } from "svelte/store";
import { user } from "../../_store.js";

/**
 * @param url_suffix path & query parameters
 * @returns A fully construct URL which can be fetched.
 */
function buildUrl(
  path_parameters: String,
  query_parameters: {} = null
): string {
  // FIXME manage frontend/backend on splitted servers
  let url = new URL(
    TargetProtocol +
      "://" +
      window.location.hostname +
      ":" +
      TargetPort +
      "/" +
      path_parameters
  );
  if (query_parameters) {
    Object.keys(query_parameters).forEach((key) =>
      url.searchParams.append(key, query_parameters[key])
    );
  }
  return url.toString();
}

///// ===== Common function to manage API calls and answers ==== /////
/**
 * Call will (most of the time) need a Bearer token => this is managed here.
 * Answers will needs some common checks & errors (like re-throw something user-friendly, re-ask token if previous expired, ...)
 */

async function doGet(url: Request | string): Promise<Response> {
  const result = await fetch(url, {
    method: "GET",
    headers: { Authorization: "Bearer " + get(user)["token"] },
  });
  const data = await result.json();

  if (result.ok) {
    return data;
  } else if (
    result.status == 401 &&
    data.detail == "Could not validate credentials"
  ) {
    // Error thrown when JWT token is KO and user needs to re-log.
    // Empty the user object so it will be catched by _layout.svelte in "logging needed" pages. This seems a correct
    // workaround as there is no guard so far in routify : https://giters.com/roxiness/routify/issues/388
    user.set({});
  }

  throw Error(data.detail);
}

async function doPost(url: Request | string): Promise<Response> {
  const result = await fetch(url, {
    method: "POST",
    headers: { Authorization: "Bearer " + get(user)["token"] },
  });
  const data = await result.json();

  if (result.status == 401 && data.detail == "Could not validate credentials") {
    // Error thrown when JWT token is KO and user needs to re-log.
    // Empty the user object so it will be catched by _layout.svelte in "logging needed" pages. This seems a correct
    // workaround as there is no guard so far in routify : https://giters.com/roxiness/routify/issues/388
    user.set({});
  }

  return data;
}

//// AUTHENT (special POST : no token)

/**
 * Get a new valid token.
 *
 * @param formData Must contains credentials (`username` & `password`) which will be sent to API.
 * @returns  A token that can be set in the `store`.
 */
export async function getToken(formData: FormData): Promise<Response> {
  const result = await fetch(buildUrl("token"), {
    method: "POST",
    body: formData,
  });
  return await result.json();
}

//// USERS ////

export async function getUserMe(): Promise<Response> {
  return await doGet(buildUrl("users/me"));
}

//// PLAYERS ////
export async function getPlayers(
  gameId: number | string,
  exclude_current_user: Boolean = true
): Promise<Response> {
  const query_params = {
    exclude_me: exclude_current_user,
  };
  return await doGet(buildUrl("games/" + gameId + "/players", query_params));
}

export async function getPlayersMe(gameId: number | string): Promise<Response> {
  return await doGet(buildUrl("games/" + gameId + "/players/me"));
}

//// GAMES ////

export async function getGames(): Promise<Response> {
  return await doGet(buildUrl("games"));
}

export async function getGameById(gameId: number | string): Promise<Response> {
  return await doGet(buildUrl("games/" + gameId));
}

//// MEMORIES ////

export async function getMemoriesMe(
  gameId: number | string
): Promise<Response> {
  return await doGet(buildUrl("games/" + gameId + "/memories"));
}

export async function getMemoryContent(
  gameId: number | string,
  memoryId: number | string
) {
  return await doGet(
    buildUrl("games/" + gameId + "/memories/" + memoryId, {
      blurred_content: true,
    })
  );
}

export async function postMemoriesDiscover(
  gameId: number | string,
  itemCode: string
) {
  return await doPost(
    buildUrl("games/" + gameId + "/discover-memory", { item_code: itemCode })
  );
}
