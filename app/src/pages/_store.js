/**
 * Store allows to share data across components.
 *
 * To persist some data in the application we are using localStorage (https://developer.mozilla.org/fr/docs/Web/API/Window/localStorage) to support browser actions like "refresh", "opening content in new tab", ...
 */

import { writable } from "svelte/store";

// Manage the link between a "stored" value (svelte) and a "local stored" value (javascript)
// Source : https://github.com/higsch/higsch.me/blob/master/content/post/2019-06-21-svelte-local-storage.md
const createWritableStore = (key, startValue) => {
  const { subscribe, set } = writable(startValue);

  return {
    subscribe,
    set,
    useLocalStorage: () => {
      const data = localStorage.getItem(key);
      if (!data) {
        set({});
      } else {
        set(JSON.parse(data));
      }

      subscribe((current) => {
        localStorage.setItem(key, JSON.stringify(current));
      });
    },
  };
};

// Stored user, empty object by default.
export const user = createWritableStore("user", {});
